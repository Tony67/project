#ifndef SERVICE_H
#define SERVICE_H

#include <QDialog>

namespace Ui {
class Service;
}

class Service : public QDialog
{
    Q_OBJECT

public:
    explicit Service(QWidget *parent = 0);
    ~Service();

private slots:
    void on_manuelStyring_clicked();

    void on_autonomStyring_clicked();

    void on_log_clicked();

private:
    Ui::Service *ui;
};

#endif // SERVICE_H
