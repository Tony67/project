#ifndef HOVEDMENU_H
#define HOVEDMENU_H

#include <QMainWindow>

namespace Ui {
class HovedMenu;
}

class HovedMenu : public QMainWindow
{
    Q_OBJECT

public:
    explicit HovedMenu(QWidget *parent = 0);
    ~HovedMenu();


private slots:

    void on_manuelStyring_clicked();

    void on_autonomStyring_2_clicked();

    void on_log_clicked();

    void on_service_clicked();

private:
    Ui::HovedMenu *ui;

};

#endif // HOVEDMENU_H
