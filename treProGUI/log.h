#ifndef LOG_H
#define LOG_H

#include <QDialog>

namespace Ui {
class Log;
}

class Log : public QDialog
{
    Q_OBJECT

public:
    explicit Log(QWidget *parent = 0);
    ~Log();

private slots:
    void on_manuelStyring_clicked();

    void on_autonomStyring_clicked();

    void on_service_clicked();

    void LogBrowser();

private:
    Ui::Log *ui;
};

#endif // LOG_H
