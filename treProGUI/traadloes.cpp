#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

#include "traadloes.h"

struct termios options;

Traadloes::Traadloes()
{
    openPort();

    setBaudRate();
}

int Traadloes::openPort()
{
    fd = open("/dev/ttyO1", O_RDWR | O_NOCTTY | O_NDELAY);

    if(fd == -1)
        perror("Open_port: Unable to open /dev/ttyO1.");
    else
        fcntl(fd, F_SETFL,0);

    return (fd);
}


void Traadloes::setBaudRate()
{
    // Get the current options for the port.
    tcgetattr(fd, &options);

    //Set the baud rates to .
    cfsetispeed(&options, B19200);
    cfsetospeed(&options, B19200);

    // Enable the receiver and set local mode.
    options.c_cflag |= (CLOCAL | CREAD);

    // Set the new options for the port.
    tcsetattr(fd, TCSANOW, &options);
}


void Traadloes::sendCommand(char* drive)
{
    int n;

    void * joyride = static_cast<void*>(drive);

    n = write(fd, joyride, 2);
    /*
    if(n < 0)
        fputs("write() of 4 bytes failed!\n", stderr);
    */
}
