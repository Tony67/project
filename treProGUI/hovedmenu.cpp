#include "hovedmenu.h"
#include "ui_hovedmenu.h"
#include "manuelstyring.h"
#include "autonomstyring.h"
#include "log.h"
#include "service.h"

HovedMenu::HovedMenu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HovedMenu)
{
    ui->setupUi(this);
}

HovedMenu::~HovedMenu()
{
    delete ui;
}

void HovedMenu::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.show();
    //mStyring.showFullScreen();
    mStyring.exec();
}

void HovedMenu::on_autonomStyring_2_clicked()
{
    AutonomStyring aStyring;
    aStyring.setModal(true);
    aStyring.show();
    //aStyring,showFullScreen();
    aStyring.exec();
}

int battery = 100;

void HovedMenu::on_service_clicked()
{
    Service sService;
    sService.setModal(true);
    sService.show();
    //sService.showFullScreen();
    sService.exec();
}

void HovedMenu::on_log_clicked()
{
    Log llog;
    llog.setModal(true);
    llog.show();
    //llog.showFullScreen();
    llog.exec();
}
