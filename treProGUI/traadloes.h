#ifndef TRAADLOES_H
#define TRAADLOES_H

class Traadloes
{
public:
    Traadloes();
    
    int openPort();

    void setBaudRate();

    void sendCommand(char*);

private:
    int fd;
};

#endif // TRAADLOES_H
