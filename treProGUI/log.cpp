#include <QFile>

#include "log.h"
#include "ui_log.h"
#include "hovedmenu.h"
#include "manuelstyring.h"
#include "autonomstyring.h"
#include "service.h"

Log::Log(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Log)
{
    ui->setupUi(this);
    LogBrowser();
}

Log::~Log()
{
    delete ui;
}


void Log::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.show();
    //mStyring.showFullScreen();
    this->close();
    mStyring.exec();
}

void Log::on_autonomStyring_clicked()
{
    AutonomStyring aStyring;
    aStyring.setModal(true);
    aStyring.show();
    //aStyring.showFullScreen();
    this->close();
    aStyring.exec();
}

void Log::on_service_clicked()
{
    Service sService;
    sService.setModal(true);
    sService.show();
    //sService.showFullScreen();
    this->close();
    sService.exec();
}

void Log::LogBrowser()
{
        QFile file("log.txt");//Filens placering
         if (!file.open(QIODevice::ReadOnly | QIODevice::Text))//Åbner filen som read only og linjeskift til \n
             ui->LogBrowser->setText("Error");//Hvis filen ikke kan åbne eller ikke findes skriver den "Error"

         QTextStream in(&file);//læser filen ind i en text stream
         while (!in.atEnd()) //Så længe der er noget i dokumentet
            {
             QString line = in.readLine(); //Læser den linje ind
             ui->LogBrowser->append(line); //Og tilføjer til textboxen
            }
        file.close();

}
