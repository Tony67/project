#include "manuelstyring.h"
#include "ui_manuelstyring.h"
#include "hovedmenu.h"
#include "autonomstyring.h"
#include "log.h"
#include "service.h"


ManuelStyring::ManuelStyring(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManuelStyring)
{
    ui->setupUi(this);
}

ManuelStyring::~ManuelStyring()
{
    delete ui;
}

void ManuelStyring::on_autonomStyring_clicked()
{
    AutonomStyring aStyring;
    aStyring.setModal(true);
    aStyring.show();
    //mStyring.showFullScreen();
    this->close();
    aStyring.exec();

}

void ManuelStyring::on_log_clicked()
{
    Log llog;
    llog.setModal(true);
    llog.show();
    //llog.showFullScreen();
    this->close();
    llog.exec();
}

void ManuelStyring::on_service_clicked()
{
    Service sService;
    sService.setModal(true);
    sService.show();
    //sService.showFullScreen();
    this->close();
    sService.exec();
}

void ManuelStyring::on_Fremad_pressed()
{
    Wireless.sendCommand("A");
}

void ManuelStyring::on_Tilbage_pressed()
{
    Wireless.sendCommand("B");
}

void ManuelStyring::on_Hoejre_pressed()
{
    Wireless.sendCommand("C");
}

void ManuelStyring::on_Venstre_pressed()
{
    Wireless.sendCommand("D");
}
