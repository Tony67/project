#include "autonomstyring.h"
#include "ui_autonomstyring.h"
#include "manuelstyring.h"
#include "hovedmenu.h"
#include "log.h"
#include "service.h"
#include "globals.h"

#include <qtconcurrentrun.h>
#include <QThread>
#include <QTimer>
#include <QString>
#include <QFile>
#include <QDateTime>
#include <QTextStream>


AutonomStyring::AutonomStyring(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AutonomStyring)
{
    ui->setupUi(this);

    ui->progressBar->setValue(battery_global); //Sætter batteri baren til seneste indlæste batteri måling
    QTimer *timer = new QTimer(this); //Starter batteri timer
        connect(timer, SIGNAL(timeout()), this, SLOT(Battery()));
        timer->start(1000);

}



AutonomStyring::~AutonomStyring()
{
    delete ui;
}


int GetBattery() // skriver en random værdi / fake batteri måler
{
 return qrand() % ((100 + 1) - 0) + 0;

}

void AutonomStyring::Battery()
{
    QFuture<int> bat =  QtConcurrent::run(GetBattery);//Læser batteri i ny tråd
    battery_global=bat;
    ui->progressBar->setValue(bat);//Sætter batteri baren
}



void AutonomStyring::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.show();
    //mStyring.showFullScreen();
    this->close();
    mStyring.exec();
}

void AutonomStyring::on_log_clicked()
{
    Log llog;
    llog.setModal(true);
    llog.show();
    //llog.showFullScreen();
    this->close();
    llog.exec();
}

void AutonomStyring::on_service_clicked()
{
    Service sService;
    sService.setModal(true);
    sService.show();
    //sService.showFullScreen();
    this->close();
    sService.exec();
}



bool freeze() //Test Kode, laver forsinkelse
{   int k =0;
    int i = 0;
        while(true)
        {
            i++;
           ( k= k * k * 0.2344523523462)/0.3252653675463555463333333*k*i;
            if (i >99999999)
                return true;
        }
}

void sut(Ui::AutonomStyring* ui)
{
   QFuture<bool> t1 =  QtConcurrent::run(freeze);

    if(t1.result())//Venter på resultatet i ny tråd så gui ikke fryser

    ui->koer->setText("Er startet"); //Ændre starte knappen, test
    ui ->textBrowser_2->append("AutonomStyring Startet"); // mere test
}



void AutonomStyring::on_koer_clicked()
{


    QFuture<void> t2 =  QtConcurrent::run(sut, ui);


}



void AutonomStyring::on_stop_clicked()
{


}


