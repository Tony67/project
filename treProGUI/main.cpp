#include "hovedmenu.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HovedMenu hMenu;
    hMenu.show();

    return a.exec();
}
