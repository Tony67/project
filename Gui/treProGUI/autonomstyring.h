#ifndef AUTONOMSTYRING_H
#define AUTONOMSTYRING_H

#include <QDialog>

namespace Ui {
class AutonomStyring;
}

class AutonomStyring : public QDialog
{
    Q_OBJECT

public:
    explicit AutonomStyring(QWidget *parent = 0);
    ~AutonomStyring();

private slots:
    void on_hovedMenu_clicked();

    void on_manuelStyring_clicked();

    void on_log_clicked();

    void on_koer_clicked();

    void on_stop_clicked();

    void on_service_clicked();

private:
    Ui::AutonomStyring *ui;
};

#endif // AUTONOMSTYRING_H
