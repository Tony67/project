#include "autonomstyring.h"
#include "ui_autonomstyring.h"
#include "manuelstyring.h"
#include "hovedmenu.h"
#include "log.h"
#include "service.h"

AutonomStyring::AutonomStyring(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AutonomStyring)
{
    ui->setupUi(this);
}

AutonomStyring::~AutonomStyring()
{
    delete ui;
}

void AutonomStyring::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.show();
    //mStyring.showFullScreen();
    this->close();
    mStyring.exec();
}

void AutonomStyring::on_log_clicked()
{
    Log llog;
    llog.setModal(true);
    llog.show();
    //llog.showFullScreen();
    this->close();
    llog.exec();
}

void AutonomStyring::on_service_clicked()
{
    Service sService;
    sService.setModal(true);
    sService.show();
    //sService.showFullScreen();
    this->close();
    sService.exec();
}


void AutonomStyring::on_koer_clicked()
{

}

void AutonomStyring::on_stop_clicked()
{

}
