#include "service.h"
#include "ui_service.h"
#include "manuelstyring.h"
#include "autonomstyring.h"
#include "log.h"

Service::Service(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Service)
{
    ui->setupUi(this);
}

Service::~Service()
{
    delete ui;
}

void Service::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.show();
    //mStyring.showFullScreen();
    this->close();
    mStyring.exec();
}

void Service::on_autonomStyring_clicked()
{
    AutonomStyring aStyring;
    aStyring.setModal(true);
    aStyring.show();
    //aStyring.showFullScreen();
    this->close();
    aStyring.exec();
}

void Service::on_log_clicked()
{
    Log llog;
    llog.setModal(true);
    llog.show();
    //llog.showFullScreen();
    this->close();
    llog.exec();
}
