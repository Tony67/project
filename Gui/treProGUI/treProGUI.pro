#-------------------------------------------------
#
# Project created by QtCreator 2014-10-22T14:25:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = treProGUI
TEMPLATE = app


SOURCES += main.cpp\
        hovedmenu.cpp \
    autonomstyring.cpp \
    log.cpp \
    manuelstyring.cpp \
    uart.cpp \
    service.cpp

HEADERS  += hovedmenu.h \
    autonomstyring.h \
    log.h \
    manuelstyring.h \
    uart.h \
    service.h

FORMS    += hovedmenu.ui \
    autonomstyring.ui \
    log.ui \
    manuelstyring.ui \
    service.ui
