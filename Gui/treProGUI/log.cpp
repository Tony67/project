#include "log.h"
#include "ui_log.h"
#include "hovedmenu.h"
#include "manuelstyring.h"
#include "autonomstyring.h"
#include "service.h"

Log::Log(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Log)
{
    ui->setupUi(this);
}

Log::~Log()
{
    delete ui;
}


void Log::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.show();
    //mStyring.showFullScreen();
    this->close();
    mStyring.exec();
}

void Log::on_autonomStyring_clicked()
{
    AutonomStyring aStyring;
    aStyring.setModal(true);
    aStyring.show();
    //aStyring.showFullScreen();
    this->close();
    aStyring.exec();
}

void Log::on_service_clicked()
{
    Service sService;
    sService.setModal(true);
    sService.show();
    //sService.showFullScreen();
    this->close();
    sService.exec();
}
