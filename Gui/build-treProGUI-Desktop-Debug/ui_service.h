/********************************************************************************
** Form generated from reading UI file 'service.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVICE_H
#define UI_SERVICE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_Service
{
public:
    QPushButton *manuelStyring;
    QTextBrowser *Forbindelse;
    QPushButton *service;
    QPushButton *log;
    QTextBrowser *textBrowser_2;
    QTextBrowser *textBrowser;
    QPushButton *autonomStyring;
    QProgressBar *progressBar;

    void setupUi(QDialog *Service)
    {
        if (Service->objectName().isEmpty())
            Service->setObjectName(QString::fromUtf8("Service"));
        Service->resize(481, 271);
        Service->setStyleSheet(QString::fromUtf8("background-color: rgb(255,255,255);"));
        manuelStyring = new QPushButton(Service);
        manuelStyring->setObjectName(QString::fromUtf8("manuelStyring"));
        manuelStyring->setGeometry(QRect(0, 30, 121, 61));
        manuelStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        Forbindelse = new QTextBrowser(Service);
        Forbindelse->setObjectName(QString::fromUtf8("Forbindelse"));
        Forbindelse->setGeometry(QRect(120, 0, 101, 31));
        service = new QPushButton(Service);
        service->setObjectName(QString::fromUtf8("service"));
        service->setGeometry(QRect(0, 210, 121, 61));
        service->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        log = new QPushButton(Service);
        log->setObjectName(QString::fromUtf8("log"));
        log->setGeometry(QRect(0, 150, 121, 61));
        log->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        textBrowser_2 = new QTextBrowser(Service);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(120, 30, 361, 241));
        textBrowser = new QTextBrowser(Service);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 121, 31));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        autonomStyring = new QPushButton(Service);
        autonomStyring->setObjectName(QString::fromUtf8("autonomStyring"));
        autonomStyring->setGeometry(QRect(0, 90, 121, 61));
        autonomStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        progressBar = new QProgressBar(Service);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(320, 0, 161, 31));
        progressBar->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        progressBar->setValue(24);
        Forbindelse->raise();
        textBrowser_2->raise();
        textBrowser->raise();
        autonomStyring->raise();
        progressBar->raise();
        manuelStyring->raise();
        log->raise();
        service->raise();

        retranslateUi(Service);

        QMetaObject::connectSlotsByName(Service);
    } // setupUi

    void retranslateUi(QDialog *Service)
    {
        Service->setWindowTitle(QApplication::translate("Service", "Dialog", 0, QApplication::UnicodeUTF8));
        manuelStyring->setText(QApplication::translate("Service", "Manuel styring", 0, QApplication::UnicodeUTF8));
        Forbindelse->setHtml(QApplication::translate("Service", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Forbinder...</p></body></html>", 0, QApplication::UnicodeUTF8));
        service->setText(QApplication::translate("Service", "Service", 0, QApplication::UnicodeUTF8));
        log->setText(QApplication::translate("Service", "Log", 0, QApplication::UnicodeUTF8));
        textBrowser_2->setHtml(QApplication::translate("Service", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" text-decoration: underline;\">Service</span></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Hvis der imod forventning skulle opst\303\245 en fejl p\303\245 denne robot, s\303\245 bedes de venligst kontakte vores service afdeling p\303\245 en af f\303\270lgen"
                        "de:</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">E-mail:	service@RoboMinion.dk</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Tlf.:	 +45 27 52 92 50</p></body></html>", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("Service", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Service</p></body></html>", 0, QApplication::UnicodeUTF8));
        autonomStyring->setText(QApplication::translate("Service", "Autonom styring", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Service: public Ui_Service {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVICE_H
