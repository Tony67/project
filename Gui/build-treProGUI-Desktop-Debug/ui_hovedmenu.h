/********************************************************************************
** Form generated from reading UI file 'hovedmenu.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOVEDMENU_H
#define UI_HOVEDMENU_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTextBrowser>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HovedMenu
{
public:
    QWidget *centralWidget;
    QPushButton *manuelStyring;
    QTextBrowser *textBrowser_2;
    QPushButton *service;
    QProgressBar *progressBar;
    QTextBrowser *textBrowser;
    QPushButton *autonomStyring_2;
    QPushButton *log;
    QTextBrowser *Forbindelse;

    void setupUi(QMainWindow *HovedMenu)
    {
        if (HovedMenu->objectName().isEmpty())
            HovedMenu->setObjectName(QString::fromUtf8("HovedMenu"));
        HovedMenu->resize(481, 271);
        HovedMenu->setStyleSheet(QString::fromUtf8("background-color: rgb(255,255,255);"));
        centralWidget = new QWidget(HovedMenu);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        manuelStyring = new QPushButton(centralWidget);
        manuelStyring->setObjectName(QString::fromUtf8("manuelStyring"));
        manuelStyring->setGeometry(QRect(0, 30, 121, 61));
        manuelStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        textBrowser_2 = new QTextBrowser(centralWidget);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(120, 30, 361, 241));
        textBrowser_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        service = new QPushButton(centralWidget);
        service->setObjectName(QString::fromUtf8("service"));
        service->setGeometry(QRect(0, 210, 121, 61));
        service->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(320, 0, 161, 31));
        progressBar->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        progressBar->setValue(24);
        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 121, 31));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        autonomStyring_2 = new QPushButton(centralWidget);
        autonomStyring_2->setObjectName(QString::fromUtf8("autonomStyring_2"));
        autonomStyring_2->setGeometry(QRect(0, 90, 121, 61));
        autonomStyring_2->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        log = new QPushButton(centralWidget);
        log->setObjectName(QString::fromUtf8("log"));
        log->setGeometry(QRect(0, 150, 121, 61));
        log->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        Forbindelse = new QTextBrowser(centralWidget);
        Forbindelse->setObjectName(QString::fromUtf8("Forbindelse"));
        Forbindelse->setGeometry(QRect(120, 0, 101, 31));
        HovedMenu->setCentralWidget(centralWidget);
        textBrowser_2->raise();
        service->raise();
        progressBar->raise();
        textBrowser->raise();
        autonomStyring_2->raise();
        log->raise();
        Forbindelse->raise();
        manuelStyring->raise();

        retranslateUi(HovedMenu);

        QMetaObject::connectSlotsByName(HovedMenu);
    } // setupUi

    void retranslateUi(QMainWindow *HovedMenu)
    {
        HovedMenu->setWindowTitle(QApplication::translate("HovedMenu", "HovedMenu", 0, QApplication::UnicodeUTF8));
        manuelStyring->setText(QApplication::translate("HovedMenu", "Manuel styring", 0, QApplication::UnicodeUTF8));
        textBrowser_2->setHtml(QApplication::translate("HovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" text-decoration: underline;\">Hoved menu</span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Velkommen til din selvstyrende robot.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0"
                        "; text-indent:0px;\">Hos RoboMinion er vi rigtige glade for at du har valgt at k\303\270be dette produkt, og vi h\303\245ber meget p\303\245 at du syntes dette er et godt produkt.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" text-decoration: underline;\">Team RoboMinions</span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Asmus</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Dennis</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">B"
                        "irk</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Simon</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Rasmus</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Simon</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Rasmus</p></body></html>", 0, QApplication::UnicodeUTF8));
        service->setText(QApplication::translate("HovedMenu", "Service", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("HovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Hoved menu</p></body></html>", 0, QApplication::UnicodeUTF8));
        autonomStyring_2->setText(QApplication::translate("HovedMenu", "Autonom styring", 0, QApplication::UnicodeUTF8));
        log->setText(QApplication::translate("HovedMenu", "Log", 0, QApplication::UnicodeUTF8));
        Forbindelse->setHtml(QApplication::translate("HovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Forbinder...</p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class HovedMenu: public Ui_HovedMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOVEDMENU_H
