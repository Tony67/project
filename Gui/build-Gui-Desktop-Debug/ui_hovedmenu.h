/********************************************************************************
** Form generated from reading UI file 'hovedmenu.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOVEDMENU_H
#define UI_HOVEDMENU_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTextBrowser>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_hovedMenu
{
public:
    QTextBrowser *textBrowser_2;
    QPushButton *manuelStyring;
    QProgressBar *progressBar;
    QPushButton *hovedMenu_2;
    QPushButton *log;
    QPushButton *autonomStyring_2;
    QTextBrowser *textBrowser;
    QTextBrowser *Forbindelse;

    void setupUi(QWidget *hovedMenu)
    {
        if (hovedMenu->objectName().isEmpty())
            hovedMenu->setObjectName(QString::fromUtf8("hovedMenu"));
        hovedMenu->resize(481, 271);
        textBrowser_2 = new QTextBrowser(hovedMenu);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(120, 30, 361, 241));
        textBrowser_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        manuelStyring = new QPushButton(hovedMenu);
        manuelStyring->setObjectName(QString::fromUtf8("manuelStyring"));
        manuelStyring->setGeometry(QRect(0, 90, 121, 61));
        manuelStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        progressBar = new QProgressBar(hovedMenu);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(320, 0, 161, 31));
        progressBar->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        progressBar->setValue(24);
        hovedMenu_2 = new QPushButton(hovedMenu);
        hovedMenu_2->setObjectName(QString::fromUtf8("hovedMenu_2"));
        hovedMenu_2->setGeometry(QRect(0, 30, 121, 61));
        hovedMenu_2->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        log = new QPushButton(hovedMenu);
        log->setObjectName(QString::fromUtf8("log"));
        log->setGeometry(QRect(0, 210, 121, 61));
        log->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        autonomStyring_2 = new QPushButton(hovedMenu);
        autonomStyring_2->setObjectName(QString::fromUtf8("autonomStyring_2"));
        autonomStyring_2->setGeometry(QRect(0, 150, 121, 61));
        autonomStyring_2->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        textBrowser = new QTextBrowser(hovedMenu);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 121, 31));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        Forbindelse = new QTextBrowser(hovedMenu);
        Forbindelse->setObjectName(QString::fromUtf8("Forbindelse"));
        Forbindelse->setGeometry(QRect(120, 0, 81, 31));

        retranslateUi(hovedMenu);

        QMetaObject::connectSlotsByName(hovedMenu);
    } // setupUi

    void retranslateUi(QWidget *hovedMenu)
    {
        hovedMenu->setWindowTitle(QApplication::translate("hovedMenu", "Form", 0, QApplication::UnicodeUTF8));
        textBrowser_2->setHtml(QApplication::translate("hovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" text-decoration: underline;\">Hoved menu</span></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Velkommen til din selvstyrende robot.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-righ"
                        "t:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Hvis der imod forventning skulle opst\303\245 en fejl p\303\245 denne robot, s\303\245 bedes de venligst skrive til vores service afdeling p\303\245 f\303\270lgende mail.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">service@awesome.com</p></body></html>", 0, QApplication::UnicodeUTF8));
        manuelStyring->setText(QApplication::translate("hovedMenu", "Manuel styring", 0, QApplication::UnicodeUTF8));
        hovedMenu_2->setText(QApplication::translate("hovedMenu", "Hoved menu", 0, QApplication::UnicodeUTF8));
        log->setText(QApplication::translate("hovedMenu", "Log", 0, QApplication::UnicodeUTF8));
        autonomStyring_2->setText(QApplication::translate("hovedMenu", "Autonom styring", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("hovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Hoved menu</p></body></html>", 0, QApplication::UnicodeUTF8));
        Forbindelse->setHtml(QApplication::translate("hovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Forbinder...</p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class hovedMenu: public Ui_hovedMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOVEDMENU_H
