#include "manuelstyring.h"
#include "ui_manuelstyring.h"
#include "hovedmenu.h"
#include "autonomstyring.h"
#include "log.h"

ManuelStyring::ManuelStyring(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManuelStyring)
{
    ui->setupUi(this);
}

ManuelStyring::~ManuelStyring()
{
    delete ui;
}

void ManuelStyring::on_hovedMenu_clicked()
{
    hovedMenu hMenu;
    //hMenu.setModal(true);
    hMenu.showFullScreen();
    //hMenu.exec();
}

void ManuelStyring::on_autonomStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.showFullScreen();
    mStyring.exec();
}

void ManuelStyring::on_log_clicked()
{
    Log llog;
    llog.setModal(true);
    llog.showFullScreen();
    llog.exec();
}

void ManuelStyring::on_Fremad_pressed()
{

}

void ManuelStyring::on_Tilbage_pressed()
{

}

void ManuelStyring::on_Hoejre_pressed()
{

}

void ManuelStyring::on_Venstre_pressed()
{

}
