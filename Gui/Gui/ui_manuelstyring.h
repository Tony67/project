/********************************************************************************
** Form generated from reading UI file 'manuelstyring.ui'
**
** Created: Mon Oct 20 12:58:15 2014
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANUELSTYRING_H
#define UI_MANUELSTYRING_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_ManuelStyring
{
public:
    QProgressBar *progressBar;
    QPushButton *manuelStyring;
    QTextBrowser *textBrowser;
    QPushButton *log;
    QPushButton *autonomStyring;
    QPushButton *Fremad;
    QPushButton *Tilbage;
    QPushButton *Venstre;
    QPushButton *Hoejre;
    QPushButton *hovedMenu;
    QTextBrowser *textBrowser_2;

    void setupUi(QDialog *ManuelStyring)
    {
        if (ManuelStyring->objectName().isEmpty())
            ManuelStyring->setObjectName(QString::fromUtf8("ManuelStyring"));
        ManuelStyring->resize(481, 271);
        ManuelStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        progressBar = new QProgressBar(ManuelStyring);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(320, 0, 161, 31));
        progressBar->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        progressBar->setValue(24);
        manuelStyring = new QPushButton(ManuelStyring);
        manuelStyring->setObjectName(QString::fromUtf8("manuelStyring"));
        manuelStyring->setGeometry(QRect(0, 90, 121, 61));
        manuelStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        textBrowser = new QTextBrowser(ManuelStyring);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 121, 31));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        log = new QPushButton(ManuelStyring);
        log->setObjectName(QString::fromUtf8("log"));
        log->setGeometry(QRect(0, 210, 121, 61));
        log->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        autonomStyring = new QPushButton(ManuelStyring);
        autonomStyring->setObjectName(QString::fromUtf8("autonomStyring"));
        autonomStyring->setGeometry(QRect(0, 150, 121, 61));
        autonomStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        Fremad = new QPushButton(ManuelStyring);
        Fremad->setObjectName(QString::fromUtf8("Fremad"));
        Fremad->setGeometry(QRect(120, 30, 361, 81));
        Tilbage = new QPushButton(ManuelStyring);
        Tilbage->setObjectName(QString::fromUtf8("Tilbage"));
        Tilbage->setGeometry(QRect(120, 190, 361, 81));
        Venstre = new QPushButton(ManuelStyring);
        Venstre->setObjectName(QString::fromUtf8("Venstre"));
        Venstre->setGeometry(QRect(120, 110, 181, 81));
        Hoejre = new QPushButton(ManuelStyring);
        Hoejre->setObjectName(QString::fromUtf8("Hoejre"));
        Hoejre->setGeometry(QRect(300, 110, 181, 81));
        hovedMenu = new QPushButton(ManuelStyring);
        hovedMenu->setObjectName(QString::fromUtf8("hovedMenu"));
        hovedMenu->setGeometry(QRect(0, 30, 121, 61));
        hovedMenu->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        textBrowser_2 = new QTextBrowser(ManuelStyring);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(120, 0, 81, 31));

        retranslateUi(ManuelStyring);

        QMetaObject::connectSlotsByName(ManuelStyring);
    } // setupUi

    void retranslateUi(QDialog *ManuelStyring)
    {
        ManuelStyring->setWindowTitle(QApplication::translate("ManuelStyring", "Dialog", 0, QApplication::UnicodeUTF8));
        manuelStyring->setText(QApplication::translate("ManuelStyring", "Manuel styring", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("ManuelStyring", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Manuel Styring</p></body></html>", 0, QApplication::UnicodeUTF8));
        log->setText(QApplication::translate("ManuelStyring", "Log", 0, QApplication::UnicodeUTF8));
        autonomStyring->setText(QApplication::translate("ManuelStyring", "Autonom styring", 0, QApplication::UnicodeUTF8));
        Fremad->setText(QApplication::translate("ManuelStyring", "Fremad", 0, QApplication::UnicodeUTF8));
        Tilbage->setText(QApplication::translate("ManuelStyring", "Bagud", 0, QApplication::UnicodeUTF8));
        Venstre->setText(QApplication::translate("ManuelStyring", "Venstre", 0, QApplication::UnicodeUTF8));
        Hoejre->setText(QApplication::translate("ManuelStyring", "H\303\270jre", 0, QApplication::UnicodeUTF8));
        hovedMenu->setText(QApplication::translate("ManuelStyring", "Hoved menu", 0, QApplication::UnicodeUTF8));
        textBrowser_2->setHtml(QApplication::translate("ManuelStyring", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Forbinder...</p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ManuelStyring: public Ui_ManuelStyring {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANUELSTYRING_H
