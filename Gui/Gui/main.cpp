#include "hovedmenu.h"
#include "manuelstyring.h"
#include "log.h"
#include "autonomstyring.h"
#include <QApplication>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    hovedMenu hMenu;
    hMenu.showFullScreen();

    return a.exec();
}

