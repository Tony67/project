#ifndef MANUELSTYRING_H
#define MANUELSTYRING_H

#include <QDialog>

namespace Ui {
class ManuelStyring;
}

class ManuelStyring : public QDialog
{
    Q_OBJECT

public:
    explicit ManuelStyring(QWidget *parent = 0);
    ~ManuelStyring();

private slots:


    void on_Fremad_pressed();

    void on_hovedMenu_clicked();

    void on_autonomStyring_clicked();

    void on_log_clicked();

    void on_Tilbage_pressed();

    void on_Hoejre_pressed();

    void on_Venstre_pressed();

private:
    Ui::ManuelStyring *ui;
};

#endif // MANUELSTYRING_H
