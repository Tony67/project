#include "autonomstyring.h"
#include "ui_autonomstyring.h"
#include "manuelstyring.h"
#include "hovedmenu.h"
#include "log.h"

AutonomStyring::AutonomStyring(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AutonomStyring)
{
    ui->setupUi(this);
}

AutonomStyring::~AutonomStyring()
{
    delete ui;
}

void AutonomStyring::on_hovedMenu_clicked()
{
    hovedMenu hMenu;
    //hMenu.setModal(true);
    hMenu.showFullScreen();
    //hMenu.exec();
}

void AutonomStyring::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.showFullScreen();
    mStyring.exec();
}

void AutonomStyring::on_log_clicked()
{
    Log llog;
    llog.setModal(true);
    llog.showFullScreen();
    llog.exec();
}

void AutonomStyring::on_koer_clicked()
{

}

void AutonomStyring::on_stop_clicked()
{

}
