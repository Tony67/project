#include "log.h"
#include "ui_log.h"
#include "hovedmenu.h"
#include "manuelstyring.h"
#include "autonomstyring.h"

Log::Log(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Log)
{
    ui->setupUi(this);
}

Log::~Log()
{
    delete ui;
}

void Log::on_hovedMenu_clicked()
{
    hovedMenu hMenu;
    //hMenu.setModal(true);
    hMenu.showFullScreen();
    //hMenu.exec();
}

void Log::on_manuelStyring_clicked()
{
    ManuelStyring mStyring;
    mStyring.setModal(true);
    mStyring.showFullScreen();
    mStyring.exec();
}

void Log::on_autonomStyring_clicked()
{
    AutonomStyring aStyring;
    aStyring.setModal(true);
    aStyring.showFullScreen();
    aStyring.exec();
}
