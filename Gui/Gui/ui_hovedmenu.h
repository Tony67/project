/********************************************************************************
** Form generated from reading UI file 'hovedmenu.ui'
**
** Created: Mon Oct 20 12:58:15 2014
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOVEDMENU_H
#define UI_HOVEDMENU_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTextBrowser>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HovedMenu
{
public:
    QTextBrowser *textBrowser;
    QProgressBar *progressBar;
    QTextBrowser *textBrowser_2;
    QPushButton *autonomStyring;
    QPushButton *log;
    QPushButton *hovedMenu;
    QPushButton *manuelStyring;
    QTextBrowser *textBrowser_3;

    void setupUi(QWidget *HovedMenu)
    {
        if (HovedMenu->objectName().isEmpty())
            HovedMenu->setObjectName(QString::fromUtf8("HovedMenu"));
        HovedMenu->resize(481, 271);
        HovedMenu->setStyleSheet(QString::fromUtf8("background-color: rgb(255,255,255);"));
        textBrowser = new QTextBrowser(HovedMenu);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 121, 31));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        progressBar = new QProgressBar(HovedMenu);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(320, 0, 161, 31));
        progressBar->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        progressBar->setValue(24);
        textBrowser_2 = new QTextBrowser(HovedMenu);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(120, 30, 361, 241));
        textBrowser_2->setStyleSheet(QString::fromUtf8("border-color: rgb(255, 255, 255);"));
        autonomStyring = new QPushButton(HovedMenu);
        autonomStyring->setObjectName(QString::fromUtf8("autonomStyring"));
        autonomStyring->setGeometry(QRect(0, 150, 121, 61));
        autonomStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        log = new QPushButton(HovedMenu);
        log->setObjectName(QString::fromUtf8("log"));
        log->setGeometry(QRect(0, 210, 121, 61));
        log->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        hovedMenu = new QPushButton(HovedMenu);
        hovedMenu->setObjectName(QString::fromUtf8("hovedMenu"));
        hovedMenu->setGeometry(QRect(0, 30, 121, 61));
        hovedMenu->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        manuelStyring = new QPushButton(HovedMenu);
        manuelStyring->setObjectName(QString::fromUtf8("manuelStyring"));
        manuelStyring->setGeometry(QRect(0, 90, 121, 61));
        manuelStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        textBrowser_3 = new QTextBrowser(HovedMenu);
        textBrowser_3->setObjectName(QString::fromUtf8("textBrowser_3"));
        textBrowser_3->setGeometry(QRect(120, 0, 81, 31));

        retranslateUi(HovedMenu);

        QMetaObject::connectSlotsByName(HovedMenu);
    } // setupUi

    void retranslateUi(QWidget *HovedMenu)
    {
        HovedMenu->setWindowTitle(QApplication::translate("HovedMenu", "HovedMenu", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("HovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Hoved menu</p></body></html>", 0, QApplication::UnicodeUTF8));
        textBrowser_2->setHtml(QApplication::translate("HovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Velkommen til din selvstyrende robot.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Hvis dette produkt imod forventing skulle have en fejl, s\303\245 bedes de venligst kontakte:</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0p"
                        "x; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">201212345@post.au.dk</p></body></html>", 0, QApplication::UnicodeUTF8));
        autonomStyring->setText(QApplication::translate("HovedMenu", "Autonom styring", 0, QApplication::UnicodeUTF8));
        log->setText(QApplication::translate("HovedMenu", "Log", 0, QApplication::UnicodeUTF8));
        hovedMenu->setText(QApplication::translate("HovedMenu", "Hoved menu", 0, QApplication::UnicodeUTF8));
        manuelStyring->setText(QApplication::translate("HovedMenu", "Manuel styring", 0, QApplication::UnicodeUTF8));
        textBrowser_3->setHtml(QApplication::translate("HovedMenu", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Forbinder...</p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class HovedMenu: public Ui_HovedMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOVEDMENU_H
