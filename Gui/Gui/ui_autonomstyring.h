/********************************************************************************
** Form generated from reading UI file 'autonomstyring.ui'
**
** Created: Mon Oct 20 12:58:15 2014
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTONOMSTYRING_H
#define UI_AUTONOMSTYRING_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_AutonomStyring
{
public:
    QProgressBar *progressBar;
    QTextBrowser *textBrowser_2;
    QTextBrowser *textBrowser;
    QPushButton *koer;
    QPushButton *stop;
    QPushButton *autonomStyring_2;
    QPushButton *log;
    QPushButton *hovedMenu;
    QPushButton *manuelStyring;
    QTextBrowser *textBrowser_3;

    void setupUi(QDialog *AutonomStyring)
    {
        if (AutonomStyring->objectName().isEmpty())
            AutonomStyring->setObjectName(QString::fromUtf8("AutonomStyring"));
        AutonomStyring->resize(481, 271);
        AutonomStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(255,255,255);"));
        progressBar = new QProgressBar(AutonomStyring);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(320, 0, 161, 31));
        progressBar->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        progressBar->setValue(24);
        textBrowser_2 = new QTextBrowser(AutonomStyring);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(120, 30, 361, 161));
        textBrowser_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        textBrowser = new QTextBrowser(AutonomStyring);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 121, 31));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        koer = new QPushButton(AutonomStyring);
        koer->setObjectName(QString::fromUtf8("koer"));
        koer->setGeometry(QRect(300, 190, 181, 81));
        stop = new QPushButton(AutonomStyring);
        stop->setObjectName(QString::fromUtf8("stop"));
        stop->setGeometry(QRect(120, 190, 181, 81));
        autonomStyring_2 = new QPushButton(AutonomStyring);
        autonomStyring_2->setObjectName(QString::fromUtf8("autonomStyring_2"));
        autonomStyring_2->setGeometry(QRect(0, 150, 121, 61));
        autonomStyring_2->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        log = new QPushButton(AutonomStyring);
        log->setObjectName(QString::fromUtf8("log"));
        log->setGeometry(QRect(0, 210, 121, 61));
        log->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        hovedMenu = new QPushButton(AutonomStyring);
        hovedMenu->setObjectName(QString::fromUtf8("hovedMenu"));
        hovedMenu->setGeometry(QRect(0, 30, 121, 61));
        hovedMenu->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        manuelStyring = new QPushButton(AutonomStyring);
        manuelStyring->setObjectName(QString::fromUtf8("manuelStyring"));
        manuelStyring->setGeometry(QRect(0, 90, 121, 61));
        manuelStyring->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 140, 0);"));
        textBrowser_3 = new QTextBrowser(AutonomStyring);
        textBrowser_3->setObjectName(QString::fromUtf8("textBrowser_3"));
        textBrowser_3->setGeometry(QRect(120, 0, 81, 31));

        retranslateUi(AutonomStyring);

        QMetaObject::connectSlotsByName(AutonomStyring);
    } // setupUi

    void retranslateUi(QDialog *AutonomStyring)
    {
        AutonomStyring->setWindowTitle(QApplication::translate("AutonomStyring", "Dialog", 0, QApplication::UnicodeUTF8));
        textBrowser_2->setHtml(QApplication::translate("AutonomStyring", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" text-decoration: underline;\">Autonom Styring</span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Din robot vil nu k\303\270re fremad indtil den m\303\270der en forhindring og vil herefter dreje 90 grader, og k\303\270re fremad.</p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Denne fremgangsm\303\270de vil den forts\303\246tte med ind"
                        "til du v\303\246lger en anden menu ude i venstre side, eller indtil du trykker p\303\245 stopknappen herunder.</p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">God forn\303\270jelse, og lad endelig ikke din robot ude af sigte!</p></body></html>", 0, QApplication::UnicodeUTF8));
        textBrowser->setHtml(QApplication::translate("AutonomStyring", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Autonom Styring</p></body></html>", 0, QApplication::UnicodeUTF8));
        koer->setText(QApplication::translate("AutonomStyring", "K\303\270r", 0, QApplication::UnicodeUTF8));
        stop->setText(QApplication::translate("AutonomStyring", "Stop", 0, QApplication::UnicodeUTF8));
        autonomStyring_2->setText(QApplication::translate("AutonomStyring", "Autonom styring", 0, QApplication::UnicodeUTF8));
        log->setText(QApplication::translate("AutonomStyring", "Log", 0, QApplication::UnicodeUTF8));
        hovedMenu->setText(QApplication::translate("AutonomStyring", "Hoved menu", 0, QApplication::UnicodeUTF8));
        manuelStyring->setText(QApplication::translate("AutonomStyring", "Manuel styring", 0, QApplication::UnicodeUTF8));
        textBrowser_3->setHtml(QApplication::translate("AutonomStyring", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Forbinder...</p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AutonomStyring: public Ui_AutonomStyring {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTONOMSTYRING_H
